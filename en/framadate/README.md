# Framadate

Framadate is an online service for planning an appointment or making a decision quickly and easily. No registration is required. It's a community free/libre software alternative to Doodle.

Here is how it works:

  1. Create a poll
  - Define dates or subjects to choose from
  - Send the poll link to your friends or colleagues
  - Discuss and make a decision

See our tutorial to get started: [Schedule an appointment quickly with Framadate](prise-en-main.html)

![Framadate screenshot](img/1.png)

## See more

- Use [Framadate](https://framadate.org)
- Contribute [to source code](https://framagit.org/framasoft/framadate/)
- Install [your own service](https://framagit.org/framasoft/framadate/wikis/home)
- Part of [the De-google-ify Internet campaign](https://degooglisons-internet.org/en/)
