# Exemple d’utilisation de Framaslides

## Manuel change le monde avec Framaslides

Manuel Dupuis-Morizeau veut changer le monde. Il se dit que la première
étape, c’est de convaincre d’autres personnes de le rejoindre dans son
envie… Et pour cela, rien de tel qu’une présentation de derrière les
fagots&nbsp;! Ne voulant pas que ses idées soient confiées aux mains de
Google Slides ou Microsoft Powerpoint 365, Manuel décide de se lancer
sur Framaslides.

Pour cela, il lui faut un compte Framaslides. C’est
facile&nbsp;: dès la page d’accueil, il clique sur le bouton «&nbsp;Se créer un
compte&nbsp;», remplit le formulaire assez classique, puis attend l’email de
confirmation (en vérifiant de temps en temps dans son dossier courriers
indésirables, sait-on jamais)
![](images/framaslide-creation-compte.png)

Une fois son compte validé, Manuel est impatient
de s’y mettre, il clique donc directement sur «&nbsp;Créer une présentation&nbsp;». Là, il découvre l’interface d’édition des diaporama de Struts.

![](images/framaslides-edition-slides.png)

-   La colonne des diapositives (1)&nbsp;;
-   Le mode expert (2) (s’il veut trifouiller du code)&nbsp;;
-   Les boutons d’ajout de contenu (3)&nbsp;;
-   Les boutons de choix des couleurs (4)&nbsp;;
-   Les vues panorama et aperçu (5).

Il décide donc de créer ses premières diapositives, ou *slides*, comme
on dit&nbsp;!

1ère slide&nbsp;: texte titre
![1ère slide&nbsp;: texte titre](images/framaslides-texte.png)

En copiant/collant l’adresse web d’un image&hellip;
![En copiant/collant l’adresse web d’un image&hellip;](images/framaslides-formulaire-image.png)

&hellip;elle s’insère dans sa 2e slide.
![&hellip;elle s’insère dans sa 2e slide.](images/framaslides-image.png)

Cela fonctionne aussi avec les vidéos (3e slide)
![Cela fonctionne aussi avec les vidéos (3e slide)](images/framaslides-video.png)

Et même les pages web/articles de blog (4e Slide)
![Et même les pages web/articles de blog (4e Slide)](images/framaslides-web.png)

Changer la couleur de fond des slides&nbsp;? Easy.
![Changer la couleur de fond des slides&nbsp;? Easy.](images/framaslides-changer-couleur.png)



Alors c’est bien gentil tout cela,
mais il ne voit toujours pas comment faire les transitions&hellip; C’est là
qu’il active le mode Panorama. Cela demande une petite gymnastique
mentale, mais il voit vite comment ça peut marcher&nbsp;!
En fait, il faut s’imaginer qu’on déplace ses slides dans l’espace.

![](images/framaslides-panorama.png)

Bon, après avoir regardé un aperçu, ce début semble
prometteur à Manuel, alors faut-il il le sauvegarde en utilisant le menu
en haut à gauche.

![](images/framaslides-menu.png)

Puis clique sur
"retourner aux présentations", dans ce même menu. Manuel se retrouve
alors devant l’interface de gestion de ses Framaslides. L’outil à l’air
assez explicite, en fait&hellip;

![](images/framasldies-interface-de-gestion-03.png)

Au centre, il retrouve ses présentations, ses modèles et ses
collaborations, chacun sous leur onglet. Et en haut à droite une barre
de recherche et d’outils qui lui permet de&nbsp;:

-   <i class="fa fa-plus"></i> créer une nouvelle présentation&nbsp;;
-   <i class="fa fa-file-text"></i> voir ses présentations (donc retourner à l’écran principal de
    son compte)&nbsp;;
-   <i class="fa fa-users"></i> gérer ses groupes de collaboration&nbsp;;
-   <i class="fa fa-picture-o"></i> gérer les images qu’il a téléversées en utilisant Framaslides&nbsp;;
-   <i class="fa fa-cog"></i> gérer les paramètres de son compte (mot de passe, etc.)&nbsp;;
-   <i class="fa fa-sign-out"></i> et se déconnecter.

Tout cela rend Manuel assez curieux, il va donc aller voir son
gestionnaire d’images, mais comme il n’en a téléchargé qu’une, cela ne
lui sert pas encore beaucoup. Il est quand même rassuré de savoir qu’il
peut en effacer à tout moment et garder la maîtrise de ses fichiers. Par
contre, Manuel a une idée brillante&hellip; se faire aider pour commencer à
changer le monde. Il décide d’aller directement créer un nouveau groupe
afin d’y inviter toute la famille Dupuis Morizeau&nbsp;!

![](images/framaslides-creation-groupe.png)

Bon, l’histoire ne dit pas si Manuel réussira à
changer le monde, mais on peut croire qu’il réussira facilement à créer
sa présentation avec d’autres membres de la famille et à la partager le
plus largement possible&nbsp;!
